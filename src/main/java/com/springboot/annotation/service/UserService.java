package com.springboot.annotation.service;

import com.springboot.annotation.entity.User;

import java.util.List;

/**
 * @author GroupiesM
 * @date 2021/07/07
 * @introduction
 */

public interface UserService {
    List<User> get();
    List<User> getByGenderB(String gender);
}
