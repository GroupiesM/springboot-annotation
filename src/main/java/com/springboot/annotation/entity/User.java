package com.springboot.annotation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author GroupiesM
 * @date 2021/07/07
 * @introduction
 */

@Data
@AllArgsConstructor//全参构造
@NoArgsConstructor//无参构造
public class User {
   private int id;//身份证
   private String name;//姓名
   private String gender;//性别
   private int age;//年龄
   //private List<String> hobbies;
   private String[] hobbies;//爱好
}