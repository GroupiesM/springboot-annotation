package com.springboot.annotation.impl;

import com.springboot.annotation.entity.User;
import com.springboot.annotation.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author GroupiesM
 * @date 2021/07/07
 * @introduction 用户服务-核心逻辑
 */
@Service
public class UserServiceImpl implements UserService {
    private static List<User> users = new ArrayList<>();
    private static List<User> returnUser;
    //静态代码块--存储用户信息
    static {
        users.add(new User(1, "张三", "男", 20, new String[]{"足球", "篮球"}));
        users.add(new User(2, "李四", "女", 21, new String[]{"舞蹈"}));
        users.add(new User(3, "林青霞", "女", 22, new String[]{"唱歌", "看电影"}));
    }
    /**
     * 查询用户
     * @return
     */
    @Override
    public List<User> get() {
        return users;
    }

    /**
     * 通过性别查找用户的方法（@PathVariable传参）
     * @param gender
     * @return
     */
    @Override
    public List<User> getByGenderB(String gender) {
        returnUser = new ArrayList<>();
        for (User user : users) {
            if (user.getGender().equals(gender)) {
                returnUser.add(user);
            }
        }
        System.out.println(returnUser);
        return returnUser;
    }
}
