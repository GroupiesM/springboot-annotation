package com.springboot.annotation.cfg;

import com.springboot.annotation.impl.AdminServiceImpl;
import com.springboot.annotation.impl.UserServiceImpl;
import com.springboot.annotation.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author GroupiesM
 * @date 2021/07/08
 * @introduction @Configuration和@Bean相关描述
 */
@Configuration
public class MyBeans {
    /**
     * 手动注入spring ioc容器，指定对象名称admin
     * @return AdminServiceImpl对象
     */
    @Bean
    public UserService admin() {
        return new AdminServiceImpl();
    }

    /**
     * 手动注入spring ioc容器，指定对象名称user
     * @return UserServiceImpl对象
     */
    @Bean
    public UserService user() {
        return new UserServiceImpl();
    }
}
