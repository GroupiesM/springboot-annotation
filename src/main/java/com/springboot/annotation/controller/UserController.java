package com.springboot.annotation.controller;

import com.springboot.annotation.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author GroupiesM
 * @date 2021/07/07
 * @introduction 4.@RequestBody相关测试
 */

@Controller
public class UserController {
    private static List<User> users = new ArrayList<>();

    //静态代码块--存储用户信息
    static {
        users.add(new User(1, "张三", "男", 20, new String[]{"足球", "篮球"}));
        users.add(new User(2, "李四", "女", 21, new String[]{"舞蹈"}));
    }

    /**
     * 查询用户（返回实体类时，不加@ResponseBody 视图解析器无法解析，报错）
     * @return
     */
    @CrossOrigin(origins = "http://localhost:8090")
    @GetMapping("/get/user/a")//http://localhost:8080/get/user/a
    public Object get() {
        return users;
    }

    /**
     * 查询用户，返回json格式 @ResponseBody
     * @return
     */
    @ResponseBody
    @CrossOrigin(origins = "http://localhost:8090")
    @GetMapping("/get/user/b")//http://localhost:8080/get/user/b
    public Object getUser() {
        return users;
    }
}
