package com.springboot.annotation.controller;

import com.springboot.annotation.entity.User;
import com.springboot.annotation.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author GroupiesM
 * @date 2021/07/07
 * @introduction
 *
 * @service 和 @autowired相关测试
 * @Autowwired 、 @Resource 、 @Qualifier相关测试
 *  调用service层
 */

@RestController
@RequestMapping("/action")
public class UserActionController {
    //以下三种写法效果相同：可以在Controller控制层同时调用一个Service接口的多个不同的实现impl
//写法1
/*    @Autowired
    @Qualifier("userServiceImpl")
    private UserService userService;

    @Autowired
    @Qualifier("adminServiceImpl")
    private UserService adminService;*/

    //写法2
/*    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private AdminServiceImpl adminService;*/

    //写法3（建议）
    @Resource(name = "userServiceImpl")
    private UserService userService;

    @Resource(name = "adminServiceImpl")
    private UserService adminService;

    //使用MyBeans中手动注入ioc的对象
    @Resource(name = "user")
    private UserService userBeanService;

    @Autowired
    @Qualifier("admin")
    private UserService adminBeanService;

    /**
     * 查询用户
     * @return 当前所有用户
     */
    @GetMapping("/get")//http://localhost:8080/action/get
    public List<User> get() {
        return userService.get();
    }

    /**
     * 通过性别查找用户的方法（@PathVariable传参）
     * @param gender 性别
     * @return 所有指定性别用户
     */
    @GetMapping("/getbygender/{gender}")   //http://localhost:8080/action/getbygender/男
    public List<User> getByGenderB(@PathVariable("gender") String gender) {
        return adminService.getByGenderB(gender);
    }
}
