package com.springboot.annotation.controller;

import com.springboot.annotation.entity.User;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author GroupiesM
 * @date 2021/07/07
 * @introduction 3.RestController注解相关测试
 *
 * \@RestController
 * \@GetMapping
 * \@PostMapping --  @RequestBody
 * \@PutMapping
 * \@DeleteMapping
 * \@PatchMapping
 */

@RestController
@RequestMapping("/user")
public class UserRestController {
    private static List<User> users = new ArrayList<>();
    private static List<User> returnUser;

    //静态代码块--存储用户信息
    static {
        users.add(new User(1, "张三", "男", 20,
                new String[]{"足球", "篮球"}));
        users.add(new User(2, "李四", "女", 21,
                new String[]{"舞蹈"}));
        users.add(new User(3, "林青霞", "女", 22,
                new String[]{"唱歌", "看电影"}));
    }

    /**
     * 查询用户
     * @return
     */
    @GetMapping("/get")//http://localhost:8080/user/get
    public List<User> get() {
        return users;
    }

    /**
     * 重新初始化数据
     * @return
     */
    @GetMapping("/init")//http://localhost:8080/user/get
    public Object init() {
        users.clear();
        users.add(new User(1, "张三", "男", 20,
                new String[]{"足球", "篮球"}));
        users.add(new User(2, "李四", "女", 21,
                new String[]{"舞蹈"}));
        users.add(new User(3, "林青霞", "女", 22,
                new String[]{"唱歌", "看电影"}));
        return users;
    }

    /**
     * 通过性别查找用户的方法（@RequestParam传参）
     * @param gender
     * @return
     */
    @GetMapping("/getbygender")//http://localhost:8080/user/getbygender
    public static Object getByGenderA(@RequestParam(value = "gender") String gender) {
        returnUser = new ArrayList<>();
        for (User user : users) {
            if (user.getGender().equals(gender)) {
                returnUser.add(user);
            }
        }
        System.out.println(returnUser);
        return returnUser;
    }

    /**
     * 通过性别查找用户的方法（@PathVariable传参）
     * @param gender
     * @return
     */
    @GetMapping("/getbygender/{gender}")//http://localhost:8080/user/getbygender/男
    public static List<User> getByGenderB(@PathVariable("gender") String gender) {
        returnUser = new ArrayList<>();
        for (User user : UserRestController.users) {
            if (user.getGender().equals(gender)) {
                returnUser.add(user);
            }
        }
        System.out.println(returnUser);
        return returnUser;
    }

    /**
     * 新增数据，返回所有用户
     * @param user
     * @return
     */
    @PostMapping("/post")//http://localhost:8080/user/post
    public List<User> addUser(@RequestBody User user) {
        users.add(user);
        return users;
    }

    /**
     * 根据id删除对应User
     * @param id
     * @return
     */
    @DeleteMapping("/delete")//http://localhost:8080/user/delete?id=3
    public List<User> deleteUser(@RequestParam(value = "id") int id) {
        Iterator<User> it = users.iterator();
        while (it.hasNext()) {
            User next = it.next();
            if (next.getId() == id) {
                it.remove();
            }
        }
        return users;
    }
}
