package com.springboot.annotation.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author GroupiesM
 * @date 2021/07/07
 * @introduction @Value相关测试
 *
 */
@RestController
@RequestMapping("/value")
public class ValueController {

    @Value("${view.page}")
    private String page;

    @Value("${local.user.username}")
    private String username;

    @Value("${local.user.password}")
    private String password;

    @GetMapping("val")//localhost:8080/value/val
    public String getValue() {
        System.out.println(page + "..." + username + "..." + password);
        return page + "..." + username + "..." + password;
    }

}
