package com.springboot.annotation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author GroupiesM
 * @date 2021/07/06
 * @introduction 2.参数相关的测试
 *
 * \@RequestMapping、@RequestParam 与 @PathVariable
 */
@Controller
public class ParamController {
    public static final String PAGE = "param";

    @RequestMapping("/param")//localhost:8080/param/id=1&type=苹果&discount=80%&price=1元
    public ModelAndView param(//必须传参
                              @RequestParam(value = "id", required = false) int id,
                              //可以不传参，但在类中要进行赋值，否则freeMarker解析页面时会报错
                              @RequestParam(value = "type", required = false) String type,
                              //可以不传参，取默认值
                              @RequestParam(value = "discount", required = false, defaultValue = "80%") String discount,
                              //可以不传参，取默认值
                              @RequestParam(value = "price", defaultValue = "5元") String price) {
        //作用类似于上面 @RequestParam注解中的属性 defaultValue="苹果"
        //不同的是 defaultValue会让required默认转为false
        if (type == null) {
            type = "苹果";
        }
        System.out.print("id = " + id);
        System.out.print(" type = " + type);
        System.out.print(" price = " + price);
        System.out.println(" discount = " + discount);

        ModelAndView view = new ModelAndView(PAGE);
        view.addObject("id", id);
        view.addObject("type", type);
        view.addObject("discount", discount);
        view.addObject("price", price);
        return view;
    }

    @RequestMapping("/param/{id}/{type}/{discount}/{price}")//localhost:8080/param/1/苹果/0.7/1元
    public ModelAndView path(@PathVariable(required = false) int id,
                             @PathVariable(required = false) String type,
                             @PathVariable(required = false) String discount,
                             @PathVariable(required = false) String price) {
        if (discount == null) {
            discount = "80%";
        }
        if (price == null) {
            price = "5元";
        }
        System.out.print("id = " + id);
        System.out.print(" type = " + type);
        System.out.print(" price = " + price);
        System.out.println(" discount = " + discount);

        ModelAndView view = new ModelAndView(PAGE);
        view.addObject("id", id);
        view.addObject("type", type);
        view.addObject("discount", discount);
        view.addObject("price", price);
        return view;
    }

    @RequestMapping(value={"/param2/{id}/{type}","/param2/{id}"})//localhost:8080/param2/1/苹果
    public ModelAndView path2(@PathVariable(required = false) int id,
                             @PathVariable(required = false) String type){
        System.out.print("id = " + id);
        System.out.print(" type = " + type);
        ModelAndView view = new ModelAndView("param2");
        view.addObject("id", id);
        view.addObject("type", type);
        return view;
    }
}