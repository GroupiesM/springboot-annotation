package com.springboot.annotation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author GroupiesM
 * @date 2021/07/06
 * @introduction 1.controller注解相关的测试
 *
 * \@Controller 与 @RequestMapping
 */
@Controller
@RequestMapping("/page")
public class ViewController {

    public static final String PAGE = "view";

    /**
     * 不带数据直接返回页面
     * @return String
     */
    @RequestMapping("/view")//url:http://localhost:8080/page/view
    public String view(){
        return PAGE;
    }

    /**
     * 带数据的返回页面
     * @return ModelAndView
     */
    @RequestMapping("/dataview")//url:http://localhost:8080/page/dataview
    public ModelAndView dataView(){
        ModelAndView view = new ModelAndView(PAGE);
        view.addObject("str1","我叫hello！");
        view.addObject("str2","我是张三！");
        return view;
    }
}
